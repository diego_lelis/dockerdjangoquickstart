## Docker Django quickstarters

This project contains two django helpers to quickstart django applications with Docker.
* One with sqlite (./django)
    
    cd django    
    ./start.sh
   
* One with PostgreSQL database (./django-postgres)

    cd django-postgres    
    ./start.sh
    
    \# Add DB config from docker-compose.yml on settings.py file
    
