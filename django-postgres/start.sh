#!/bin/bash
echo Enter project name:
read projectname
sudo docker-compose run web django-admin startproject $projectname .
sudo chown -R $USER:$USER . # if docker is not enabled to run without sudo
docker-compose up